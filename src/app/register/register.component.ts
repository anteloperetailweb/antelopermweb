import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import {UserService} from '../service/user.service';
import {DataserviceService} from '../service/dataservice.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  angForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private dataService:DataserviceService) { 

    this.angForm = this.fb.group({
      username: ['', [Validators.required,Validators.minLength(1), Validators.email]],
      password: ['', Validators.required],
      usertype: ['', Validators.required]
    });
  }
  ngOnInit(): void {
  }

  postdata(angForm1)
  {
    this.dataService.userregistration(angForm1.value.username,angForm1.value.password,angForm1.value.usertype)
      .pipe(first())
      .subscribe(
          data => {
              this.router.navigate(['login']);
          },
          error => {
          });
  }
  get username() { return this.angForm.get('username'); }
  get password() { return this.angForm.get('password'); }
  get usertypr() { return this.angForm.get('usertypr'); }

}
