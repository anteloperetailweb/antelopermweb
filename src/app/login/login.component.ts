import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators } from "@angular/forms";
import {Router} from "@angular/router";
import { first } from 'rxjs/operators';
import {DataserviceService} from "../service/dataservice.service";
import {UserService} from '../service/user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  //invalidLogin: boolean = false;
  angForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private dataService: DataserviceService, private userservice:UserService) { 

    this.angForm = this.fb.group({
 
      email: ['', [Validators.required,Validators.minLength(1)]],
      password: ['', Validators.required]
    });
  }

  // onSubmit() {
  //   if (this.loginForm.invalid) {
  //     return;
  //   }
  //   const loginPayload = {
  //     username: this.loginForm.controls.username.value,
  //     password: this.loginForm.controls.password.value,
  //     aksi: "login"
  //   }


  //   this.userservice.login(loginPayload, "proses-api.php").subscribe(data => {
  //     // if(data.status === 200) {
  //       if(data) {
  //     window.localStorage.setItem("session_storage", JSON.parse(data));
  //           this.router.navigate(['list-user']);
  //         }else {
  //           this.invalidLogin = true;
  //           alert(data);
  //         }
  //   });
    // this.apiService.login(loginPayload).subscribe(data => {
    //   debugger;
    //   if(data.status === 200) {
    //     window.localStorage.setItem('token', data.result.token);
    //     this.router.navigate(['list-user']);
    //   }else {
    //     this.invalidLogin = true;
    //     alert(data.message);
    //   }
    // });
  // }

  ngOnInit() {
  
  }
  postdata(angForm1)
  {
    const loginPayload = {
          username: angForm1.value.email,
          password: angForm1.value.password,
          aksi: "login"
        }

    this.dataService.userlogin(loginPayload)
      .pipe(first())
      .subscribe(
          data => {
                const redirect = this.dataService.redirectUrl ? this.dataService.redirectUrl : '/addmenu';
                this.router.navigate([redirect]);
 
          },
          error => {
              alert("User name or password is incorrect")
          });
  }
  get email() { return this.angForm.get('email'); }
  get password() { return this.angForm.get('password'); }


}
