export class Usermodule {
  public user_id: string;
  public username: string;
  public password: string;
  public usertype: string;
  public customer_id: string;
  public name_customer: string;
  public desc_customer: string;
  public created_at: string;

  constructor(
    user_id?: string,
    username?: string,
    password?: string,
    usertype?: string,
    customer_id?: string,
    name_customer?: string,
    desc_customer?: string,
    created_at?: string
  ) {
    this.user_id = user_id;
    this.username = username;
    this.password = password;
    this.usertype = usertype;
    this.customer_id = customer_id;
    this.name_customer = name_customer;
    this.desc_customer = desc_customer;
    this.created_at = created_at;
  }
}
