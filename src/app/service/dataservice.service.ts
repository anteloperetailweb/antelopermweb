import { Injectable , Output, EventEmitter } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usermodule } from '../model/user.model';

//Headers and File
const headers = new HttpHeaders({
  "Content-Type": "application/json; charset=utf-8",
});
const file = "proses-api.php";

@Injectable({
  providedIn: 'root'
})
export class DataserviceService {
  redirectUrl: string;
    // server: string = "http://rm.antelopeventures.com/server_api/"; 
server: string = "http://localhost/IONIC4_CRUD_LOGINREGIS_PHP_MYSQL/server_api/";
  @Output() getLoggedInName: EventEmitter<any> = new EventEmitter();

  constructor(private httpClient : HttpClient) { }

  public userlogin(credentials) {
    return this.httpClient.post<any>(this.server + file, JSON.stringify(credentials))
        .pipe(map(Usermodule => {
            this.setToken(Usermodule["result"].username);
            this.getLoggedInName.emit(true);
            return Usermodule;
        }));
}

public userregistration(username,password,usertype) {
  return this.httpClient.post<any>(this.server + file, {username,password,usertype})
      .pipe(map(Usermodule => {
          return Usermodule;
      }));
}

// public updateuserdetails(id,name,email,pwd,mobile) {
//   return this.httpClient.post<any>(this.baseUrl + '/updateuser.php', { id,name,email,pwd,mobile })
//     .pipe(map(Usermodule => {
//           return Usermodule;
//       }));
 
// }
// removeEmployee(empid: number): Observable<Usermodule[]> {
//   return this.httpClient.delete<Usermodule[]>(this.baseUrl+'/deletedata.php?empid='+empid );
// }
public getUserId(empid: number): Observable<Usermodule[]>
  {
    return this.httpClient.get<Usermodule[]>(
      this.server + '/getdataone.php?'+ 'empid=' + empid 
      );
  }
 
getAllUsers(body) : Observable<Usermodule[] > {

  return this.httpClient.post<Usermodule[]>(this.server + file, body);
}

//token
setToken(token: string) {
  localStorage.setItem('token', token);
}
 
getToken() {
  return localStorage.getItem('token');
}
 
deleteToken() {
  localStorage.removeItem('token');
}
 
isLoggedIn() {
  const usertoken = this.getToken();
  if (usertoken != null) {
    return true
  }
  return false;
}

}
