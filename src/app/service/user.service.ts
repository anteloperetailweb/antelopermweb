import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";


//Headers and File
const headers = new HttpHeaders({
  "Content-Type": "application/json; charset=utf-8",
});
const file = "proses-api.php";



@Injectable({
  providedIn: 'root'
})
export class UserService {
  // server: string = "http://rm.antelopeventures.com/server_api/"; 
server: string = "http://localhost/IONIC4_CRUD_LOGINREGIS_PHP_MYSQL/server_api/";

  constructor(private http: HttpClient) {}

  login(credentials, file) {
    // Normally make a POST request to your APi with your login credentials

    return this.http
      .post(this.server + file, JSON.stringify(credentials), {
        headers: headers
      })
      .pipe(
        map((res) => {
          return JSON.stringify(res);
        })
      );
  }

}
