import { Component, OnInit } from '@angular/core';
import {DataserviceService} from "../../service/dataservice.service";
import { Usermodule } from '../../model/user.model';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  users: Usermodule[];
  cat:number;
  limit: number = 13; // LIMIT GET PERDATA
  start: number = 0;

  constructor(private dataService: DataserviceService,private router:Router) { }
 
  ngOnInit() {
    this.getuserdetails();
 
  }

  getuserdetails()
{

  let body = {
    aksi : 'getdata',
    limit : this.limit,
    start : this.start,
  };
  this.dataService.getAllUsers(body).subscribe((response: any) =>
    {
if(response){

this.users = response.result;

}
    });
}
deleteuserdetails(user:Usermodule)
{
  // this.dataService.removeEmployee(user.Id)
  // .subscribe( data => {
  //   //this.users = this.users.filter(u => u !== user);
  //   this.getuserdetails();
  // })
 
}
updateUser(user: Usermodule): void {
  window.localStorage.removeItem("editId");
  window.localStorage.setItem("editId", user.user_id.toString());
  this.router.navigate(['edit']);
};
addUser(): void {
  this.router.navigate(['create']);
};
}
